Source: gnocchi
Section: net
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Michal Arbet <michal.arbet@ultimum.io>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools (>= 99~),
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-setuptools-scm,
 python3-sphinx,
Build-Depends-Indep:
 alembic,
 libpq-dev,
 postgresql,
 postgresql-server-dev-all,
 python3-boto3,
 python3-botocore (>= 1.5),
 python3-cachetools,
 python3-cotyledon (>= 1.5.0),
 python3-coverage (>= 3.6),
 python3-daiquiri,
 python3-doc8,
 python3-fixtures,
 python3-future (>= 0.15),
 python3-gabbi (>= 1.37.0),
 python3-iso8601,
 python3-jsonpatch,
 python3-keystoneclient (>= 1:1.6.0),
 python3-keystonemiddleware (>= 4.0.0),
 python3-lz4 (>= 0.9.0),
 python3-mock,
 python3-monotonic,
 python3-mysqldb,
 python3-numpy,
 python3-os-testr,
 python3-oslo.config (>= 1:3.22.0),
 python3-oslo.db (>= 4.29.0),
 python3-oslo.middleware (>= 3.22.0),
 python3-oslo.policy,
 python3-paste,
 python3-pastedeploy,
 python3-pecan,
 python3-prettytable,
 python3-protobuf,
 python3-psycopg2,
 python3-pymysql,
 python3-pyparsing (>= 2.2.0),
 python3-pytimeparse,
 python3-redis,
 python3-six,
 python3-snappy,
 python3-sphinx-bootstrap-theme,
 python3-sphinx-rtd-theme,
 python3-sphinxcontrib.httpdomain,
 python3-sqlalchemy,
 python3-sqlalchemy-utils (>= 0.32.14),
 python3-stevedore,
 python3-swiftclient (>= 3.1.0),
 python3-sysv-ipc,
 python3-tenacity (>= 4.6.0),
 python3-testresources,
 python3-testscenarios,
 python3-testtools (>= 0.9.38),
 python3-tooz (>= 1.38),
 python3-ujson,
 python3-voluptuous,
 python3-webob,
 python3-webtest (>= 2.0.16),
 python3-werkzeug,
 python3-wsgi-intercept (>= 1.4.1),
 python3-yaml,
 subunit (>= 0.0.18),
 testrepository,
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/openstack-team/services/gnocchi
Vcs-Git: https://salsa.debian.org/openstack-team/services/gnocchi.git
Homepage: https://gnocchi.xyz/

Package: gnocchi-api
Architecture: all
Depends:
 adduser,
 debconf,
 gnocchi-common (= ${binary:Version}),
 lsb-base,
 python3-keystoneclient,
 python3-openstackclient,
 python3-pastescript,
 q-text-as-data,
 uwsgi-plugin-python3,
 ${misc:Depends},
 ${python3:Depends},
Description: Metric as a Service - API daemon
 Gnocchi is a service for managing a set of resources and storing metrics about
 them, in a scalable and resilient way. Its functionalities are exposed over an
 HTTP REST API.
 .
 This package contains the API server.

Package: gnocchi-common
Architecture: all
Depends:
 adduser,
 dbconfig-common,
 debconf,
 python3-gnocchi (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
Description: Metric as a Service - common files
 Gnocchi is a service for managing a set of resources and storing metrics about
 them, in a scalable and resilient way. Its functionalities are exposed over an
 HTTP REST API.
 .
 This package contains the common files.

Package: gnocchi-metricd
Architecture: all
Depends:
 gnocchi-common (= ${binary:Version}),
 lsb-base,
 ${misc:Depends},
 ${python3:Depends},
Description: Metric as a Service - metric daemon
 Gnocchi is a service for managing a set of resources and storing metrics about
 them, in a scalable and resilient way. Its functionalities are exposed over an
 HTTP REST API.
 .
 This package contains the metric daemon.

Package: gnocchi-statsd
Architecture: all
Depends:
 gnocchi-common (= ${binary:Version}),
 lsb-base,
 ${misc:Depends},
 ${python3:Depends},
Description: Metric as a Service - statsd daemon
 Gnocchi is a service for managing a set of resources and storing metrics about
 them, in a scalable and resilient way. Its functionalities are exposed over an
 HTTP REST API.
 .
 This package contains the statsd daemon.

Package: python3-gnocchi
Section: python
Architecture: all
Depends:
 alembic,
 python3-boto3,
 python3-botocore (>= 1.5),
 python3-cachetools,
 python3-cotyledon (>= 1.5.0),
 python3-daiquiri,
 python3-future (>= 0.15),
 python3-iso8601,
 python3-jsonpatch,
 python3-keystoneclient (>= 1:1.6.0),
 python3-keystonemiddleware (>= 4.0.0),
 python3-lz4 (>= 0.9.0),
 python3-monotonic,
 python3-numpy,
 python3-oslo.config (>= 1:3.22.0),
 python3-oslo.db (>= 4.29.0),
 python3-oslo.middleware (>= 3.22.0),
 python3-oslo.policy,
 python3-oslosphinx (>= 2.2.0.0),
 python3-paste,
 python3-pastedeploy,
 python3-pbr,
 python3-pecan,
 python3-prettytable,
 python3-protobuf,
 python3-psycopg2,
 python3-pymysql,
 python3-pyparsing (>= 2.2.0),
 python3-pytimeparse,
 python3-redis,
 python3-six,
 python3-snappy,
 python3-sqlalchemy,
 python3-sqlalchemy-utils (>= 0.32.14),
 python3-stevedore,
 python3-swiftclient (>= 3.1.0),
 python3-tenacity (>= 4.6.0),
 python3-tooz (>= 1.38),
 python3-ujson,
 python3-voluptuous,
 python3-webob,
 python3-werkzeug,
 python3-yaml,
 ${misc:Depends},
 ${python3:Depends},
Breaks:
 python-gnocchi,
Replaces:
 python-gnocchi,
Suggests:
 gnocchi-doc,
Description: Metric as a Service - Python 3.x
 Gnocchi is a service for managing a set of resources and storing metrics about
 them, in a scalable and resilient way. Its functionalities are exposed over an
 HTTP REST API.
 .
 This package contains the Python 3.x module.
